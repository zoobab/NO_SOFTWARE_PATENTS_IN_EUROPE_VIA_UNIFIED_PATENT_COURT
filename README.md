# NO Software Patents in Europe via the Unified Patent Court!

### We are protesting against software patents in Europe.

### Software patents are an insult to the profession, steal our jobs, and are promoting extortion practices like patent trolls.

### After Brexit, we are asking our politicians to renegotiate the Unified Patent Court (UPC) and put back the European Court of Justice (CJEU) as having a say on patent law, and software patents in particular (ex art6-8, veto from the UK and Mr Cameron, pushed by the british patent industry and GSK, a large vaccine manufacturer).

### We oppose Germany's move to try to put the UPC treaty into force ('fait accompli') while London is still in the text.

### The UPC will raise the court fees for a simple dispute from 600EUR (average of court fees in most EU countries) to 12.000EUR (20.000EUR - 40% rebate for small companies), while the current national systems are already too expensive because of expensive lawyers fees.

----
